// Copyright (c) 2023 Boomi, Inc.
package com.boomi.connector.veeva.auth.cache;

import com.boomi.connector.util.ConnectorCache;

import java.time.LocalDateTime;

/**
 * Class for caching a Veeva session across multiple connector invocations. The expiration date is verified and possibly
 * extended when {@link #isValid()} is invoked.
 * <p>
 * Veeva sessions are generated by calling the Rest API auth endpoint and are active as long as a request is made within
 * the maximum inactive session duration. The latter parameter is configured in the Veeva UI and is exposed in the
 * connector as "Session Timeout" to calculate the expiration time. The sessions can be extended until a maximum period
 * of 48 hours, defined by Veeva.
 * <p>
 * Session padding was added to avoid the edge case of a session that becomes invalid while a request is made.
 */
public class SessionCache extends ConnectorCache<String> {

    private static final long SESSION_MAX_DURATION_MINUTES = 48L * 60L;
    private static final long SESSION_PADDING_MINUTES = 1L;

    private final String _sessionId;
    private final long _timeToLive;
    private final LocalDateTime _maxExpirationTime;

    private LocalDateTime _expirationTime;

    public SessionCache(String key, String sessionId, long timeToLive) {
        super(key);
        _sessionId = sessionId;
        _timeToLive = timeToLive;
        _maxExpirationTime = subtractPadding(LocalDateTime.now().plusMinutes(SESSION_MAX_DURATION_MINUTES));
        _expirationTime = subtractPadding(LocalDateTime.now().plusMinutes(timeToLive));
    }

    private static LocalDateTime subtractPadding(LocalDateTime dateTime) {
        return dateTime.minusMinutes(SESSION_PADDING_MINUTES);
    }

    public String getSessionId() {
        return _sessionId;
    }

    LocalDateTime getExpirationTime() {
        return _expirationTime;
    }

    /**
     * Verifies if the session is valid by checking its expiration time. If it is valid, the expiration time is extended
     * one minute less than the configured timeout, until reaching the maximum expiration time allowed by the service,
     * which is 48 hours.
     *
     * @return {@code true} if the contained session is not expired, {@code false} otherwise
     */
    @Override
    public boolean isValid() {
        if (_expirationTime == null || LocalDateTime.now().isAfter(_expirationTime)) {
            return false;
        }

        LocalDateTime newExpirationTime = subtractPadding(LocalDateTime.now().plusMinutes(_timeToLive));
        _expirationTime = newExpirationTime.isBefore(_maxExpirationTime) ? newExpirationTime : _maxExpirationTime;

        return true;
    }
}
