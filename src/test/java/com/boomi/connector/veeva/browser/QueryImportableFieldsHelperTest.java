// Copyright (c) 2024 Boomi, LP
package com.boomi.connector.veeva.browser;

import com.boomi.connector.api.ui.BrowseField;
import com.boomi.connector.api.ui.DataType;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Properties;

public class QueryImportableFieldsHelperTest {

    @Test
    void buildDocumentVersionFilterOptionsTest() {
        Properties fieldsProperties = QueryImportableFieldsHelper.loadImportableFieldProperties();
        BrowseField field = QueryImportableFieldsHelper.buildDocumentVersionFilterOptions(fieldsProperties);

        Assertions.assertEquals(DataType.STRING, field.getType());
        Assertions.assertEquals("VERSION_FILTER_OPTIONS", field.getId());
        Assertions.assertEquals("Version Filter Options", field.getLabel());
        Assertions.assertEquals("LATEST_VERSION", field.getDefaultValue());
        Assertions.assertEquals("Choose which document versions to return.", field.getHelpText());
        Assertions.assertEquals("ALL_MATCHING_VERSIONS", field.getAllowedValues().get(1).getValue());
        Assertions.assertEquals("LATEST_MATCHING_VERSION", field.getAllowedValues().get(2).getValue());
    }
}
