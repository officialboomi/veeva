// Copyright (c) 2024 Boomi, Inc.
package com.boomi.connector.veeva.operation.query;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.QueryFilter;
import com.boomi.connector.api.Sort;
import com.boomi.connector.testutil.MutablePropertyMap;
import com.boomi.connector.testutil.QueryFilterBuilder;
import com.boomi.connector.testutil.QueryGroupingBuilder;
import com.boomi.connector.testutil.QuerySimpleBuilder;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class VQLQueryBuilderTest {

    private final OperationContext _context = mock(OperationContext.class, RETURNS_DEEP_STUBS);
    private final MutablePropertyMap _operationProperties = new MutablePropertyMap();

    VQLQueryBuilderTest() {
        when(_context.getOperationProperties()).thenReturn(_operationProperties);
    }

    @Test
    void buildQuery() {
        _operationProperties.put("MAXDOCUMENTS", 28L);
        _operationProperties.put("PAGESIZE", 2L);

        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(Collections.singletonList("id"));

        QueryFilter qf = new QueryFilterBuilder(QueryGroupingBuilder.and(new QuerySimpleBuilder("id", "ne_number", "0"),
                new QuerySimpleBuilder("name__v", "LIKE", "Tr%"))).toFilter();

        Sort sort1 = new Sort();
        sort1.setProperty("id");
        sort1.setSortOrder("ASC");
        qf.withSort(sort1);

        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);

        assertEquals("q=SELECT id FROM test_object WHERE id != 0  AND name__v LIKE 'Tr%25'  ORDER BY id ASC MAXROWS 28 "
                     + "PAGESIZE 2", queryBuilder.buildVQLQuery(qf));
    }

    @Test
    void buildQuery2() {
        _operationProperties.put("MAXDOCUMENTS", 50L);
        _operationProperties.put("PAGESIZE", 10L);

        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(Collections.singletonList("id"));

        QueryFilter qf = new QueryFilterBuilder(QueryGroupingBuilder.and(new QuerySimpleBuilder("id", "eq_number", "0"),
                new QuerySimpleBuilder("name__v", "CONTAINS", "T"))).toFilter();
        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);

        assertEquals("q=SELECT id FROM test_object WHERE id = 0  AND name__v CONTAINS ('T') MAXROWS 50 PAGESIZE 10",
                queryBuilder.buildVQLQuery(qf));
    }

    @ParameterizedTest
    @ValueSource(longs = { 0L, -1L })
    void buildQueryWithUnlimitedPageSize(long pageSize) {
        _operationProperties.put("PAGESIZE", pageSize);

        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(Collections.singletonList("id"));

        QueryFilter qf = new QueryFilterBuilder(QueryGroupingBuilder.and(new QuerySimpleBuilder("id", "eq_number", "0"),
                new QuerySimpleBuilder("name__v", "CONTAINS", "T"))).toFilter();
        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);

        assertEquals("q=SELECT id FROM test_object WHERE id = 0  AND name__v CONTAINS ('T')",
                queryBuilder.buildVQLQuery(qf));
    }

    @ParameterizedTest
    @ValueSource(longs = { 0L, -1L })
    void buildQueryWithUnlimitedMaxDocuments(long maxDocuments) {
        _operationProperties.put("MAXDOCUMENTS", maxDocuments);

        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(Collections.singletonList("id"));

        QueryFilter qf = new QueryFilterBuilder(QueryGroupingBuilder.and(new QuerySimpleBuilder("id", "eq_number", "0"),
                new QuerySimpleBuilder("name__v", "CONTAINS", "T"))).toFilter();
        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);

        assertEquals("q=SELECT id FROM test_object WHERE id = 0  AND name__v CONTAINS ('T')",
                queryBuilder.buildVQLQuery(qf));
    }

    @Test
    void buildQueryWithMaxDocuments() {
        _operationProperties.put("MAXDOCUMENTS", 42L);

        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(Collections.singletonList("id"));

        QueryFilter qf = new QueryFilterBuilder(QueryGroupingBuilder.and(new QuerySimpleBuilder("id", "eq_number", "0"),
                new QuerySimpleBuilder("name__v", "CONTAINS", "T"))).toFilter();
        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);

        assertEquals("q=SELECT id FROM test_object WHERE id = 0  AND name__v CONTAINS ('T') MAXROWS 42",
                queryBuilder.buildVQLQuery(qf));
    }

    @Test
    void buildQueryWithPageSize() {
        _operationProperties.put("PAGESIZE", 42L);

        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(Collections.singletonList("id"));

        QueryFilter qf = new QueryFilterBuilder(QueryGroupingBuilder.and(new QuerySimpleBuilder("id", "eq_number", "0"),
                new QuerySimpleBuilder("name__v", "CONTAINS", "T"))).toFilter();
        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);

        assertEquals("q=SELECT id FROM test_object WHERE id = 0  AND name__v CONTAINS ('T') PAGESIZE 42",
                queryBuilder.buildVQLQuery(qf));
    }

    @Test
    void buildQueryWithFindStatement() {
        _operationProperties.put("FIND", "Example1");

        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(Collections.singletonList("id"));

        QueryFilter qf = new QueryFilterBuilder(new QuerySimpleBuilder("name__v", "LIKE", "Tr%")).toFilter();

        Sort sort1 = new Sort();
        sort1.setProperty("id");
        sort1.setSortOrder("ASC");
        qf.withSort(sort1);

        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);

        assertEquals("q=SELECT id FROM test_object FIND ('Example1')  WHERE name__v LIKE 'Tr%25'  ORDER BY id ASC",
                queryBuilder.buildVQLQuery(qf));
    }

    @Test
    void buildQueryWithQuotedFindStatement() {
        _operationProperties.put("FIND", "'Example1 OR Example2'");

        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(Collections.singletonList("id"));

        QueryFilter qf = new QueryFilterBuilder(new QuerySimpleBuilder("name__v", "LIKE", "Tr%")).toFilter();

        Sort sort1 = new Sort();
        sort1.setProperty("id");
        sort1.setSortOrder("ASC");
        qf.withSort(sort1);

        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);

        assertEquals(
                "q=SELECT id FROM test_object FIND ('Example1 OR Example2')  WHERE name__v LIKE 'Tr%25'  ORDER BY id "
                + "ASC", queryBuilder.buildVQLQuery(qf));
    }

    @Test
    void buildQueryWithAllMatchingVersions() {
        _operationProperties.put("VERSION_FILTER_OPTIONS", "ALL_MATCHING_VERSIONS");

        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(Collections.singletonList("id"));

        QueryFilter qf = new QueryFilterBuilder(QueryGroupingBuilder.and(new QuerySimpleBuilder("id", "eq_number", "0"),
                new QuerySimpleBuilder("name__v", "IN", "T"))).toFilter();
        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);

        assertEquals("q=SELECT id FROM ALLVERSIONS test_object WHERE id = 0  AND name__v IN (T) ",
                queryBuilder.buildVQLQuery(qf));
    }

    @Test
    void buildQueryWithLatestMatchingVersion() {
        _operationProperties.put("VERSION_FILTER_OPTIONS", "LATEST_MATCHING_VERSION");

        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(Collections.singletonList("id"));

        QueryFilter qf = new QueryFilterBuilder(QueryGroupingBuilder.and(new QuerySimpleBuilder("id", "gt_number", "0"),
                QueryGroupingBuilder.and(new QuerySimpleBuilder("description__v", "le_date", "20/12/2024"),
                        new QuerySimpleBuilder("name__v", "ge_string", "three")))).toFilter();
        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);

        assertEquals("q=SELECT LATESTVERSION id FROM ALLVERSIONS test_object WHERE id > 0  AND ( description__v <= "
                     + "'20/12/2024'  AND name__v >= 'three' )", queryBuilder.buildVQLQuery(qf));
    }

    @Test
    void buildQueryWithLatestVersion() {
        _operationProperties.put("VERSION_FILTER_OPTIONS", "LATEST_VERSION");

        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(Collections.singletonList("id"));

        QueryFilter qf = new QueryFilterBuilder(new QuerySimpleBuilder("id", "lt_number", "0")).toFilter();

        Sort sort1 = new Sort();
        sort1.setProperty("id");
        sort1.setSortOrder("ASC");
        Sort sort2 = new Sort();
        sort2.setProperty("name__v");
        sort2.setSortOrder("DESC");

        qf.withSort(sort1, sort2);

        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);

        assertEquals("q=SELECT id FROM test_object WHERE id < 0  ORDER BY id ASC, name__v DESC",
                queryBuilder.buildVQLQuery(qf));
    }

    @Test
    void buildQueryWithoutFilterArgument() {
        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(Collections.singletonList("id"));

        QueryFilter qf = new QueryFilterBuilder(new QuerySimpleBuilder("id", "eq_number", "")).toFilter();

        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);
        assertThrows(ConnectorException.class, () -> queryBuilder.buildVQLQuery(qf));
    }

    @Test
    void buildQueryWithMultipleFilterArguments() {
        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(Collections.singletonList("id"));

        QueryFilter qf = new QueryFilterBuilder(new QuerySimpleBuilder("id", "eq_number", "1", "2")).toFilter();

        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);
        assertThrows(IllegalStateException.class, () -> queryBuilder.buildVQLQuery(qf));
    }

    @Test
    void buildQueryWithEmptyFilterPropertyName() {
        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(Collections.singletonList("id"));

        QueryFilter qf = new QueryFilterBuilder(new QuerySimpleBuilder("", "eq_number", "1")).toFilter();

        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);
        assertThrows(ConnectorException.class, () -> queryBuilder.buildVQLQuery(qf));
    }

    @Test
    void buildQueryWithoutFields() {
        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(Collections.emptyList());

        QueryFilter qf = new QueryFilterBuilder(new QuerySimpleBuilder("", "eq_number", "1")).toFilter();

        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);
        assertThrows(ConnectorException.class, () -> queryBuilder.buildVQLQuery(qf));
    }

    @Test
    void buildQueryWithMultipleFields() {
        _operationProperties.put("MAXDOCUMENTS", 28L);
        _operationProperties.put("PAGESIZE", 2L);

        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(Arrays.asList("id", "description__v"));

        QueryFilter qf = new QueryFilterBuilder(QueryGroupingBuilder.and(new QuerySimpleBuilder("id", "ne_number", "0"),
                new QuerySimpleBuilder("name__v", "LIKE", "Tr%"))).toFilter();

        Sort sort1 = new Sort();
        sort1.setProperty("id");
        sort1.setSortOrder("ASC");
        qf.withSort(sort1);

        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);

        assertEquals(
                "q=SELECT id,description__v FROM test_object WHERE id != 0  AND name__v LIKE 'Tr%25'  ORDER BY id ASC"
                + " MAXROWS 28 " + "PAGESIZE 2", queryBuilder.buildVQLQuery(qf));
    }

    @Test
    void buildQueryWithNestedFields() {
        _operationProperties.put("MAXDOCUMENTS", 28L);
        _operationProperties.put("PAGESIZE", 2L);

        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(Arrays.asList("id", "cdx_rule_set__vr/initiator_role_label__v"));

        QueryFilter qf = new QueryFilterBuilder(QueryGroupingBuilder.and(new QuerySimpleBuilder("id", "ne_number", "0"),
                new QuerySimpleBuilder("name__v", "LIKE", "Tr%"))).toFilter();

        Sort sort1 = new Sort();
        sort1.setProperty("id");
        sort1.setSortOrder("ASC");
        qf.withSort(sort1);

        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);

        assertEquals("q=SELECT id,cdx_rule_set__vr.initiator_role_label__v FROM test_object WHERE id != 0  AND name__v "
                     + "LIKE 'Tr%25'  ORDER BY id ASC MAXROWS 28 " + "PAGESIZE 2", queryBuilder.buildVQLQuery(qf));
    }

    @Test
    void buildQueryWithSimpleSubquery() {
        _operationProperties.put("MAXDOCUMENTS", 28L);
        _operationProperties.put("PAGESIZE", 2L);

        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(
                Arrays.asList("id", "agreement_transfers__vr__Subquery/created_date__v"));

        QueryFilter qf = new QueryFilterBuilder(QueryGroupingBuilder.and(new QuerySimpleBuilder("id", "ne_number", "0"),
                new QuerySimpleBuilder("name__v", "LIKE", "Tr%"))).toFilter();

        Sort sort1 = new Sort();
        sort1.setProperty("id");
        sort1.setSortOrder("ASC");
        qf.withSort(sort1);

        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);

        assertEquals(
                "q=SELECT id,(SELECT created_date__v FROM agreement_transfers__vr) FROM test_object WHERE id != 0  "
                + "AND name__v LIKE 'Tr%25'  ORDER BY id ASC MAXROWS 28 PAGESIZE 2", queryBuilder.buildVQLQuery(qf));
    }

    @Test
    void buildQueryWithMultipleFieldsInSingleSubquery() {
        _operationProperties.put("MAXDOCUMENTS", 28L);
        _operationProperties.put("PAGESIZE", 2L);

        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(
                Arrays.asList("id", "agreement_transfers__vr__Subquery/created_date__v",
                        "agreement_transfers__vr__Subquery/finish_time__v"));

        QueryFilter qf = new QueryFilterBuilder(QueryGroupingBuilder.and(new QuerySimpleBuilder("id", "ne_number", "0"),
                new QuerySimpleBuilder("name__v", "LIKE", "Tr%"))).toFilter();

        Sort sort1 = new Sort();
        sort1.setProperty("id");
        sort1.setSortOrder("ASC");
        qf.withSort(sort1);

        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);

        assertEquals(
                "q=SELECT id,(SELECT created_date__v,finish_time__v FROM agreement_transfers__vr) FROM test_object "
                + "WHERE id != 0  AND name__v LIKE 'Tr%25'  ORDER BY id ASC MAXROWS 28 PAGESIZE 2",
                queryBuilder.buildVQLQuery(qf));
    }

    @Test
    void buildQueryWithMultipleSubqueries() {
        _operationProperties.put("MAXDOCUMENTS", 28L);
        _operationProperties.put("PAGESIZE", 2L);

        when(_context.getObjectTypeId()).thenReturn("test_object");
        when(_context.getSelectedFields()).thenReturn(
                Arrays.asList("id", "agreement_transfers__vr__Subquery/created_date__v",
                        "agreement_transfers__vr__Subquery/finish_time__v",
                        "cdx_agreement_activity_items__vr__Subquery/component__v"));

        QueryFilter qf = new QueryFilterBuilder(QueryGroupingBuilder.and(new QuerySimpleBuilder("id", "ne_number", "0"),
                new QuerySimpleBuilder("name__v", "LIKE", "Tr%"))).toFilter();

        Sort sort1 = new Sort();
        sort1.setProperty("id");
        sort1.setSortOrder("ASC");
        qf.withSort(sort1);

        VQLQueryBuilder queryBuilder = new VQLQueryBuilder(_context);

        assertEquals("q=SELECT id,(SELECT created_date__v,finish_time__v FROM agreement_transfers__vr),(SELECT "
                     + "component__v FROM cdx_agreement_activity_items__vr) FROM test_object WHERE id != 0  AND "
                     + "name__v " + "LIKE 'Tr%25'  ORDER BY id ASC MAXROWS 28 PAGESIZE 2",
                queryBuilder.buildVQLQuery(qf));
    }
}